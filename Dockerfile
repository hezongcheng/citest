#docker build -t registry.cn-hangzhou.aliyuncs.com/hzc_dev/citest .
#docker run --name citest -p 8081:8000 -d registry.cn-hangzhou.aliyuncs.com/hzc_dev/citest:latest
FROM registry.cn-hangzhou.aliyuncs.com/hzc_dev/php8.3-swoole:latest 
WORKDIR /var/www/html
COPY . /var/www/html
CMD composer install && php think run

